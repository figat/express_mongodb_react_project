const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
const db = require("./config/db");
const server = express();
const Port = process.env.PORT || 8080;

//Routes require
const indexRoutes = require("./routes/api");

//Middleware
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());
server.use(morgan("tiny"));
server.use(cors());
server.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

//DB connect
db.dbConnection();

// Use Routes
server.use(indexRoutes);

server.listen(Port, () => {
  console.log(`Server running on port:${Port}`);
});
