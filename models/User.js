const mongoose = require("mongoose");

// Create Schema

const User = new mongoose.Schema({
  userName: { type: String, required: true },
  userEmail: { type: String, required: true, trim: true },
  userPassword: { type: String, required: true }
});

module.exports = mongoose.model("Post", User);
