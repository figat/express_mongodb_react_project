const express = require("express");
router = express.Router();
const { GetMainPath } = require("../../controllers/index");
router.get("/", GetMainPath);

module.exports = router;
