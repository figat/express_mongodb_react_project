module.exports = [
  {
    _id: "5dae6e17b930ef35dcc6467d",
    age: 30,
    eyeColor: "blue",
    name: "Gracie Woods",
    gender: "female",
    company: "SYNKGEN",
    email: "graciewoods@synkgen.com",
    phone: "+1 (962) 598-3393",
    address: "376 Plaza Street, Zeba, New Jersey, 5505"
  },
  {
    _id: "5dae6e179f6e4f8b98756e40",
    age: 27,
    eyeColor: "blue",
    name: "Atkins Rivers",
    gender: "male",
    company: "DOGNOSIS",
    email: "atkinsrivers@dognosis.com",
    phone: "+1 (981) 401-2821",
    address: "776 Emmons Avenue, Turah, West Virginia, 597"
  },
  {
    _id: "5dae6e17fff59cdc411c51dc",
    age: 25,
    eyeColor: "brown",
    name: "Shelly Gutierrez",
    gender: "female",
    company: "ZILLACOM",
    email: "shellygutierrez@zillacom.com",
    phone: "+1 (967) 550-2934",
    address: "280 Havens Place, Mahtowa, Nevada, 8650"
  },
  {
    _id: "5dae6e175daca784311ac102",
    age: 23,
    eyeColor: "blue",
    name: "Lydia Workman",
    gender: "female",
    company: "ORBAXTER",
    email: "lydiaworkman@orbaxter.com",
    phone: "+1 (916) 600-2959",
    address: "154 Prospect Place, Golconda, New Mexico, 4963"
  },
  {
    _id: "5dae6e171a32241af3ad83d8",
    age: 25,
    eyeColor: "blue",
    name: "Wagner Dunlap",
    gender: "male",
    company: "URBANSHEE",
    email: "wagnerdunlap@urbanshee.com",
    phone: "+1 (973) 482-3404",
    address: "737 Danforth Street, Harold, Arkansas, 4708"
  },
  {
    _id: "5dae6e1769428da51a458391",
    age: 20,
    eyeColor: "green",
    name: "Yolanda Ayala",
    gender: "female",
    company: "ECRAZE",
    email: "yolandaayala@ecraze.com",
    phone: "+1 (915) 505-3081",
    address: "802 Kensington Walk, Whitehaven, Louisiana, 5454"
  }
];
