import React, { Component } from "react";
import "./App.css";
import axios from "axios";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: []
    };
  }

  componentDidMount() {
    axios
      .get(`http://localhost:8080`)
      .then(res => {
        const { data } = res.data;
        data.map(() => {
          return this.setState({ data });
        });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const { data } = this.state;

    return (
      <div className="App">
        {data.map((users, _id) => (
          <div className="userInfo" key={_id}>
            <li className="listItems">
              <span className="listDesc">Name: </span>
              {users.name}
            </li>
            <li className="listItems">
              <span className="listDesc">Gender: </span>
              {users.gender}
            </li>
            <li className="listItems">
              <span className="listDesc">Company: </span>
              {users.company}
            </li>
            <li className="listItems">
              <span className="listDesc">Email: </span>
              {users.email}
            </li>
            <li className="listItems">
              <span className="listDesc">Address: </span>
              {users.address}
            </li>
            <li className="listItems">
              <span className="listDesc">Age: </span>
              {users.age}
            </li>
          </div>
        ))}
      </div>
    );
  }
}

export default App;
